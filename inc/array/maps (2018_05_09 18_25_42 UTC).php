<?php

// Empty Maps array
$maps = [];

// Adding maps to the array
$maps[001] = [
  "title" => "Bank",
  "fullimage" => "/siege_site/assets/full/bank.jpg",
  "icon" => "/siege_site/assets/icon/bank_icon.jpg",
  "date" => "01-12-2015",
  "description" => "An iconic encounter for LA SWAT forces, this map depicts an assault on a major bank. The focus is on providing a sense of progression for attackers as they make their way through progressively more fortified areas of the building. ",
  "location" => "United State of America",
  "ctu" => "LA Swat",
  "floors" => [
    "1" => [
      "title" => "Basement",
      "floorplan" => "/siege_site/assets/floorplans/bank-0.jpg"
    ],
    "2" => [
      "title" => "First Floor",
      "floorplan" => "/siege_site/assets/floorplans/bank-1.jpg"
    ],
    "3" => [
      "title" => "Second Floor",
      "floorplan" => "/siege_site/assets/floorplans/bank-2.jpg"
    ],
    "4" => [
      "title" => "Roof",
      "floorplan" => "/siege_site/assets/floorplans/bank-3.jpg"
    ],
  ],
];

$maps[002] = [
  "title" => "Bartlett U.",
  "fullimage" => "/siege_site/assets/full/bartlett.jpg",
  "icon" => "/siege_site/assets/icon/bartlett_icon.jpg",
  "date" => "",
  "description" => "Team Rainbow has been called to assist local law enforcement and regain control of Bartlett University in Cambridge, Massachusetts. This map offers an asymmetric layout with breathing room between hot zones. Navigate the facilities barricades and blast through the brick walls that have protected the leaders of tomorrow. ",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[003] = [
  "title" => "Border",
  "fullimage" => "/siege_site/assets/full/border.jpg",
  "icon" => "/siege_site/assets/icon/border_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[004] = [
  "title" => "Chalet",
  "fullimage" => "/siege_site/assets/full/chalet.jpg",
  "icon" => "/siege_site/assets/icon/chalet_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[005] = [
  "title" => "Club House",
  "fullimage" => "/siege_site/assets/full/club-house.jpg",
  "icon" => "/siege_site/assets/icon/clubhouse_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[006] = [
  "title" => "Coastline",
  "fullimage" => "/siege_site/assets/full/coastline.png",
  "icon" => "/siege_site/assets/icon/coastline_icon.png",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[007] = [
  "title" => "Consulate",
  "fullimage" => "/siege_site/assets/full/consulate.jpg",
  "icon" => "/siege_site/assets/icon/consulate_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[008] = [
  "title" => "Favela",
  "fullimage" => "/siege_site/assets/full/favela.jpg",
  "icon" => "/siege_site/assets/icon/favela_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[009] = [
  "title" => "Hereford",
  "fullimage" => "/siege_site/assets/full/hereford.jpg",
  "icon" => "/siege_site/assets/icon/hereford_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[010] = [
  "title" => "House",
  "fullimage" => "/siege_site/assets/full/house.jpg",
  "icon" => "/siege_site/assets/icon/house_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[011] = [
  "title" => "Kafe Dostoyevsky",
  "fullimage" => "/siege_site/assets/full/kafe.jpg",
  "icon" => "/siege_site/assets/icon/kafe_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[012] = [
  "title" => "Kanal",
  "fullimage" => "/siege_site/assets/full/kanal.jpg",
  "icon" => "/siege_site/assets/icon/kanal_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[013] = [
  "title" => "Oregon",
  "fullimage" => "/siege_site/assets/full/oregon.jpg",
  "icon" => "/siege_site/assets/icon/oregon_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[014] = [
  "title" => "Plane",
  "fullimage" => "/siege_site/assets/full/plane.jpg",
  "icon" => "/siege_site/assets/icon/plane_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[015] = [
  "title" => "Skyscraper",
  "fullimage" => "/siege_site/assets/full/skyscraper.jpg",
  "icon" => "/siege_site/assets/icon/skyscraper_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[016] = [
  "title" => "Theme Park",
  "fullimage" => "/siege_site/assets/full/theme-park.jpg",
  "icon" => "/siege_site/assets/icon/theme-park_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];

$maps[017] = [
  "title" => "Yacht",
  "fullimage" => "/siege_site/assets/full/yacht.jpg",
  "icon" => "/siege_site/assets/icon/yacht_icon.jpg",
  "date" => "",
  "description" => "",
  "location" => "",
  "ctu" => "",
  "floors" => [
    "1" => "Basement",
    "2" => "First Floor",
    "3" => "Second Floor",
  ],
];
