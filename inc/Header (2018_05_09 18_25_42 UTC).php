<!DOCTYPE html>
<html>
  <head>
    <title>RS6 Siege Tips - <?php echo $pageTitle; ?></title>
    <meta charset="UTF-8" />
    <meta name="robots" content="index,follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Delving deep into the murky depths of the philosophical world hidden within the realm of games." />
    <meta name="keywords" content="gaming, Gaming, Philosophy, philosophical, The Last of Us, Soma, Bioshock, The Talos Principle, Games, games" />
    <meta name="robots" content="index,follow" />
    <meta http-equiv="Cache-control" content="public">
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Delving deep into the murky depths of the philosophical world hidden within the realm of games." />
    <link rel="shortcut icon" type="image/x-icon" href="Images/icon.ico">
    <link rel="stylesheet" href="/siege_site/inc/css/style.css" type="text/css">
  </head>
  <body>
    <nav class="flex_row">
      <a href="/siege_site" class="span2">
      <img class="header_icon" src="/siege_site/assets/full/header_logo.png" />
    </a>
      <ul class="flex_row">
        <li><a href="">Test</a></li>
        <li><a href="">Test</a></li>
        <li><a href="">Test</a></li>
      </ul>
    </nav>
    <main>
